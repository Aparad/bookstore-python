from flask import jsonify
from scripts import db
from scripts.models.book import Book
from scripts.models.author import Author
from scripts.models.category import Category
from scripts.models.categoriesbooks import CategoriesBooks
import json

class BookDTO():
    """ Book DTO class. """
    def __init__(self, bookObject = None):
        if bookObject:
            self.id = bookObject.id
            self.title = bookObject.title
            self.author_id = bookObject.author_id
            self.author = Author.query.filter_by(id=bookObject.author_id).first().name
            self.description = bookObject.description
            categories_ids = list(
                map(lambda x: x.category_id,
                    CategoriesBooks.query.filter_by(book_id=bookObject.id).all()
                    )
            )
            self.categories_names = list(
                map(
                    lambda x: x.name,
                    Category.query.filter(Category.id.in_(categories_ids)).all()
                )
            )

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)
