from scripts import db
from scripts.models.book import Book
from scripts.models.author import Author
from scripts.dto.bookdto import BookDTO
import requests
from flask import Blueprint, request, render_template, Markup

""" Performs operations required to import books from Google database. """

importer_blueprint = Blueprint("importer", __name__)


@importer_blueprint.route("/searchtoimport", methods=['POST', 'GET'])
def search_books_to_import():
    """ Renders page with list of first 10 books according to the search criteria. """
    if request.method == 'POST':
        try:
            title = request.form["title"]
            r = 'https://www.googleapis.com/books/v1/volumes?q='
            if title:
                r += '+intitle:{}'.format(title)

            author = request.form["author"]
            if author:
                r += '+inauthor:{}'.format(author)

            r += '&projection=lite&printType=books'

            resp = requests.get(r).json()
            if 'error' in resp.keys():
                return render_template("importbooks.html", message='Could not find any books with this criteria!')
            bookslist = map_g_json_to_bookdto(resp)
            return render_template("importbooks.html", bookstoimport=bookslist, title=Markup(title), author=Markup(author))

        except Exception as e:
            return render_template("importbooks.html", message='Error occured!\n{error}'.format(error=str(e)))
    else:
        return render_template("importbooks.html")


@importer_blueprint.route("/importbooks", methods=['POST'])
def add_books_to_db():
    """ Performs book search, and then imports the results. """
    if request.method == 'POST':
        try:
            title = request.form["title"]
            r = 'https://www.googleapis.com/books/v1/volumes?q='
            if title:
                r += '+intitle:{}'.format(title)

            author = request.form["author"]
            if author:
                r += '+inauthor:{}'.format(author)

            r += '&projection=lite&printType=books'

            resp = requests.get(r).json()
            imported_books_amount = add_books_to_db_from_g_json(resp)
            return render_template("importbooks.html", message='Imported {} books successfully!'.format(imported_books_amount))

        except Exception as e:
            return render_template("importbooks.html", message='Error occured!\n{error}'.format(error=str(e)))
    else:
        return render_template("importbooks.html")


def add_books_to_db_from_g_json(google_json):
    """ Adds first 10 books according to the search criteria.
        Adds a book only if all required fields are supplied. """
    bookslist = []
    for item in google_json['items']:
        _newBook = Book()
        book_info = item['volumeInfo']

        _newBook.title = book_info['title']

        if 'description' in book_info.keys():
            _newBook.description = book_info['description']

        if 'authors' in book_info.keys():
            author = book_info['authors'][0]
        else:
            continue
        is_author_in_db = Author.query.filter_by(name=author).scalar() is not None
        if is_author_in_db:
            _newBook.author_id = Author.query.filter_by(name=author).first().id
        else:
            _newAuthor = Author()
            _newAuthor.name = author
            db.session.add(_newAuthor)
            db.session.commit()
            _newBook.author_id = Author.query.filter_by(name=_newAuthor.name).first().id

        db.session.add(_newBook)
        db.session.commit()
        bookslist.append(_newBook)
    return len(bookslist)


def map_g_json_to_bookdto(google_json):
    """ Maps received JSON to BookDTO. """
    bookslist = []
    for item in google_json['items']:
        _newBookDTO = BookDTO()
        book_info = item['volumeInfo']

        _newBookDTO.title = book_info['title']

        if 'description' in book_info.keys():
            _newBookDTO.description = book_info['description']
        else:
            _newBookDTO.description = 'This book has no description currently!'

        if 'authors' in book_info.keys():
            _newBookDTO.author = book_info['authors'][0]
        else:
            _newBookDTO.author = 'Author unknown'
        bookslist.append(_newBookDTO)

    return bookslist