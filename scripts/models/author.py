from scripts import db


class Author(db.Model):
    """ Author model.
        Properties:
        * Books - one-to-many,
        * Name,
        * Surname.
    """
    __tablename__ = "authors"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=200))
    authorbooks = db.relationship("Book", backref="author")
