from scripts import db


class Category(db.Model):
    """ Category model. Many-to-many assoc. between Book and Category table.
    """
    __tablename__ = "categories"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=100))
    books_ids = db.relationship("Book", secondary='categoriesbooks', back_populates='categories_ids')
