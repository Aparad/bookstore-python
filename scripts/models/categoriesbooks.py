from scripts import db



class CategoriesBooks(db.Model):
    """ Category-Book association table. """
    __tablename__ = "categoriesbooks"
    __table_args__ = (
        db.PrimaryKeyConstraint('book_id', 'category_id'),
    )
    book_id = db.Column(db.Integer, db.ForeignKey('books.id'))
    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
