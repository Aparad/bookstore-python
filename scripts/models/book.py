from scripts import db


class Book(db.Model):
    """ Book model.
        Properties:
        * Author - one-to-many,
        * Category - one-to-many,
        * Description,
        * Title.
    """
    __tablename__ = "books"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(length=500))
    description = db.Column(db.String(length=5000))
    author_id = db.Column(db.Integer, db.ForeignKey('authors.id'))
    categories_ids = db.relationship("Category", secondary='categoriesbooks', back_populates='books_ids')

