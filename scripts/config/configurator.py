from bs4 import BeautifulSoup
from sys import exit


class configurator:
    """ Helper class for configuration files. """

    _config_location = ".\config\config.xml"

    @classmethod
    def read_config(cls):
        try:
            with open(configurator._config_location) as cfg:
                configurator.config = BeautifulSoup(cfg.read(), features="html.parser")
        except IOError as e:
            print("Missing configuration file?")
            exit(2)

    @classmethod
    def get_config(cls):
        if hasattr(configurator, 'config'):
            return configurator.config
        else:
            configurator.read_config()
            return configurator.config
