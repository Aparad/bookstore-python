from flask import Flask, render_template
from scripts.config.configurator import configurator
from flask_sqlalchemy import SQLAlchemy


configurator.read_config()
config = configurator.get_config()

host = config.mysql.host.contents[0]
db_name = config.mysql.database.contents[0]
user = config.mysql.user.contents[0]
password = config.mysql.password.contents[0]


app = Flask(__name__, instance_relative_config=True, template_folder='views')
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'some_really_long_random_string_here'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = None
try:
    _URI = "mysql+mysqlconnector://{username}:{password}@{host}:3306/{dbname}".format(
        username=user,
        password=password,
        host=host,
        dbname=db_name
    )
    app.config['SQLALCHEMY_DATABASE_URI'] = _URI
    db = SQLAlchemy(app)
except Exception as e:
    print("Could not connect to {db} database!".format(db=db_name))
    raise e


from scripts.dao.bookdao import book_blueprint
app.register_blueprint(book_blueprint)

from scripts.importer.importer import importer_blueprint
app.register_blueprint(importer_blueprint)


@app.route('/')
def hello():
    return render_template('index.html')

