from scripts import db
from flask import Blueprint, request, render_template, jsonify
from scripts.models.book import Book
from scripts.models.author import Author
from scripts.models.category import Category
from scripts.models.categoriesbooks import CategoriesBooks
from scripts.dto.bookdto import BookDTO
""" Allows executing db operations related with books."""

book_blueprint = Blueprint('book', __name__)

def get_books_by_author_name(author_name):
    author_id = Author.query.filter_by(name=author_name).first().id
    books = Book.query.filter_by(author_id=author_id).all()
    return books


def get_books_by_title(title):
    book = Book.query.filter_by(title=title).first()
    return book


@book_blueprint.route('/book/<id>')
def get_book_by_id(id):
    book = Book.query.filter_by(id=id).first()
    if book:
        bookdto = BookDTO(book)
        return render_template('booklayout.html', book=bookdto.__dict__)
    else:
        return render_template('booklayout.html', message='No book with this id!')


@book_blueprint.route('/category/<getid>')
def get_books_by_category_id(getid):
    books_ids = list(map(lambda x: x.book_id, CategoriesBooks.query.filter_by(category_id=getid).all()))
    books = list(map(lambda x: x.title, Book.query.filter(Book.id.in_(books_ids)).all()))
    return ", ".join(books)


@book_blueprint.route('/add-book', methods=['GET', 'POST'])
def add_book():
    if request.method == 'POST':
        try:
            title = request.form["title"]
            author = request.form["author"]
            category = request.form["category"]

            if not title or not author or not category:
                return render_template("addbook.html", message='Enter all informations!')

            _new_book = Book()
            _new_book.title = title

            is_author_in_db = Author.query.filter_by(name=author).scalar() is not None
            if is_author_in_db:
                _new_book.author_id = Author.query.filter_by(name=author).first().id
            else:
                _newAuthor = Author()
                _newAuthor.name = author
                db.session.add(_newAuthor)
                db.session.commit()
                _new_book.author_id = Author.query.filter_by(name=_newAuthor.name).first().id

            db.session.add(_new_book)
            db.session.commit()

            is_category_in_db = Category.query.filter_by(name=category).scalar() is not None
            if is_category_in_db:
                category_id = Category.query.filter_by(name=category).first().id
                _new_categorybook = CategoriesBooks()
                _new_categorybook.book_id = _new_book.id
                _new_categorybook.category_id = category_id
            else:
                _new_category = Category()
                _new_category.name = category
                db.session.add(_new_category)
                db.session.commit()
                category_id = Category.query.filter_by(name=category).first().id
                _new_categorybook = CategoriesBooks()
                _new_categorybook.book_id = _new_book.id
                _new_categorybook.category_id = category_id

            db.session.add(_new_categorybook)
            db.session.commit()

            return render_template("addbook.html", message='Added {book} to the database successfully!'.format(book=title))

        except Exception as e:
            return render_template("addbook.html", message='Error occured!\n{error}'.format(error=str(e)))
    else:
        return render_template("addbook.html")


@book_blueprint.route('/search', methods=['POST', 'GET'])
def search_for_books():
    if request.method == 'POST':
        try:
            booklist = []
            if not request.form['author'] and not request.form['category'] and not request.form['title']:
                booklist = Book.query.all()

            if request.form['title']:
                booklist = Book.query.filter(Book.title.like('%{}%'.format(request.form['title']))).all()

            if request.form['author']:
                author_id = list(map(lambda x: x.id, Author.query.filter(
                    Author.name.like('%{}%'.format(request.form['author']))
                ).all()))
                if booklist:
                    booklist = list(filter(lambda x: x.id == author_id, booklist))
                else:
                    booklist = Book.query.filter(Book.author_id.in_(author_id)).all()

            if request.form['category'] and Category.query.filter_by(name=request.form['category']).scalar() is not None:
                category_id = Category.query.filter(Category.name.filter_by(name=request.form['category'])).first().id

                if booklist:
                    booklist = list(filter(lambda x: x.id == category_id, booklist))
                else:
                    books_ids = list(map(lambda x: x.book_id, CategoriesBooks.query.filter_by(category_id=category_id).all()))
                    booklist = Book.query.filter(Book.id.in_(books_ids)).all()

            if not booklist:
                return render_template('search.html', message='No books found!')
            return render_template('search.html', books=list(map(lambda x: (x.id, x.title), booklist)))

        except Exception as e:
            return render_template("search.html", message='Error occured!\n{error}'.format(error=str(e)))
    else:
        return render_template("search.html")
